# Umbau der Seite auf eine moderne Struktur

## HTML

- Im Wurzelelement `html` soll die Sprache des Dokumentinhalts angegeben sein. Das dient dem Browser an sich dazu, zum Beispiel Trennungsregeln oder eine eventuell aktivierte Rechtschreibprüfung passend zur angegebenen Sprache anzuwenden. Weiterhin dient es Screenreadern zur Benutzung der korrekten Sprache.
- Die neu eingebauten Elemente sind nicht in XML-Notation eingefügt. Diese Notation (zum Beispiel `<br />`) lässt sich aber, entsprechend den bekannten Regeln von XML/XHTML, weiterhin benutzen.
- Im Element `head` mit den Metaangaben zum Dokument können ein paar Vereinfachungen vorgenommen werden.
    1. Die Angabe der Zeichenkodierung kann auf `<meta charset="utf-8">` verkürzt werden.
    1. Mit `<meta name="viewport" content="width=device-width, initial-scale=1.0">` wird Mobilbrowsern mitgeteilt, die Seite nicht im Desktopmodus darzustellen.
    1. Entferne das Attribut `type="text/css"` vom `<meta>`-Element zum Laden des Stylesheets.
- Ersetze `<div id="header">`mit `<header>`.
- Verpacke die beiden Hauptnavigationslisten im Seitenkopf und -fuß in `<nav>`.
- Ersetze `<div id="navbar">` mit `<nav>`.
- Ersetze `<div id="content">` mit `<main>`.
- Ersetze `<div id="footer">` mit `<footer>`.
- Verpacke *alle* Gruppen von Links zur Navigation auf der Seite und im Projekt in Listen. Dazu gehört auch die Eine Seite-Vor-Und-Zurück-Und-Nach-Oben-Navigation.
- Ersetze die für die Formatierung der Streifen in Tabellenzeilen benutzten Klassen durch die Zählung in CSS per `:nth-child`.
